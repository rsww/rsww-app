from datetime import datetime
import urllib3
import copy
import logging
import os
import copy
from json import dumps
import time
from locust import HttpUser, task, between
from locust.exception import StopUser

# 0. Create 1000 Users
# 1. Upload 50 plików do analizy,
# 2. Odpytanie 1000 razy o listę plików do analizy,
# 3. Upload 100 plików jako dokumenty wzorcowe,
# 4. Odpytanie 1000 razy o listę plików wzorcowych,
# 5. Wysłanie 2000 żądań analizy dokumentu,
# 6. Wysłanie 2000 żądań o statusy analiz.
urllib3.disable_warnings()
class RswwStageOneTester(HttpUser):
    def on_start(self):
        self.client.headers["Content-Type"] = "application/json"
        self.create_users()
        self.register("new_user_123456")
        self.login()
        self.upload_first_file()

    def create_users(self):
        for i in range(1000):
            self.register(f"user_{i}")

    def register(self, username):

        body = {
            "login": username,
            "password": "U$er123123",
            "email": "maciek@example.com",
            "firstname" : "Krzysztof",
            "lastname" : "Swiecicki"
        }
        self.client.request("POST", "/api/auth/register", data=dumps(body), headers=self.client.headers, verify=False)

    def login(self):

        body = {
            "login": "new_user_123456",
            "password": "U$er123123"
        }
        response = self.client.request("POST", "/api/auth/login", data=dumps(body), headers=self.client.headers, verify=False)
        token = response.json().get('data').get('token')
        logging.info(token)
        if response.status_code == 200:
            self.client.headers["Authorization"] = f"Bearer {token}"
    
    def upload_first_file(self):
        headers = dict()
        headers["Authorization"] = self.client.headers["Authorization"]
        headers["Content-Type"] = None
        for i in range(1):
            filename = f"{i}.txt"
            files = {
                "files": (f"{filename}", open(f"base/{filename}", 'rb'), "text/plain")
            }
            data = {'name':f"{filename}", 'isBase':True}
            response = self.client.post("/api/repositories/", data=data, files=files, headers=headers, verify=False)
        self.client.repo_uuid = response.json().get('data').get('repositoryId')

    @task()
    def load_test(self):
        logging.info("start " + datetime.now().strftime("%H:%M:%S"))
        self.ask_for_files_for_analysis()
        self.ask_for_base_files()
        self.ask_for_document_analysis()
        self.ask_for_status_of_analyses()
        self.upload_base_file()
        self.upload_repo_file()
        logging.info("end " + datetime.now().strftime("%H:%M:%S"))

         
    def ask_for_files_for_analysis(self):
        for i in range(1000):
            response = self.client.get("/api/repositories/", headers=self.client.headers, verify=False)

    def ask_for_base_files(self):
        for i in range(1000):
            response = self.client.get("/api/repositories/", headers=self.client.headers, verify=False)

    def ask_for_document_analysis(self):
        #logging.info(self.client.repo_uuid)
        #logging.info("esssa")
        for i in range(2000):
            json = '{"repositoryrefs" :' +f'["{self.client.repo_uuid}"]' + '}'
            response = self.client.post("/api/reports/", json, headers=self.client.headers, verify=False)

    def ask_for_status_of_analyses(self):
        for i in range(2000):
            response = self.client.get("/api/reports/" , headers=self.client.headers, verify=False)

    def upload_base_file(self):
        headers = dict()
        headers["Authorization"] = self.client.headers["Authorization"]
        headers["Content-Type"] = None
        for i in range(100):
            filename = f"{i}.txt"
            files = {
                "files": (f"{filename}", open(f"base/{filename}", 'rb'), "text/plain")
            }
            data = {'name':f"{filename}", 'isBase':True}
            response = self.client.post("/api/repositories/", data=data, files=files, headers=headers, verify=False)

    def upload_repo_file(self):
        headers = dict()
        headers["Authorization"] = self.client.headers["Authorization"]
        headers["Content-Type"] = None
        for i in range(50):
            filename = f"{i}.txt"
            files = {
                "files": (f"{filename}", open(f"files/{filename}", 'rb'), "text/plain")
            }
            data = {'name':f"{filename}", 'isBase':False}
            response = self.client.post("/api/repositories/", data=data, files=files, headers=headers, verify=False)


