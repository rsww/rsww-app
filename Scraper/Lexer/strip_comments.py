from .lex import lex
import argparse
import os


def _strip_comments(source):
    tokens = ('CHAR',
              'NEWLINE',
              'LINECOMMENT',
              'BEGINCOMMENT',
              'ENDCOMMENT')

    states = (
        ('blockcomment', 'exclusive'),
    )

    def t_LINECOMMENT(t):
        r"//.*"
        pass

    def t_BEGINCOMMENT(t):
        r"/\*"
        t.lexer.begin('blockcomment')

    def t_blockcomment_ENDCOMMENT(t):
        r"\*/"
        t.lexer.begin('INITIAL')

    def t_blockcomment_CHAR(t):
        r'.'
        pass

    def t_blockcomment_NEWLINE(t):
        r"\n"
        pass

    def t_CHAR(t):
        r"."
        return t

    def t_NEWLINE(t):
        r"\n"
        return t

    # For bad characters, we just skip over it
    def t_ANY_error(t):
        t.lexer.skip(1)

    lexer = lex()
    lexer.input(source)
    return "".join([tok.value for tok in lexer])


def strip_comments(filename):
    # It reads entire file to the memory
    try:
        with open(filename, 'r', errors='ignore') as f:
            source = f.read()
    except OSError:
        return

    with open(filename, 'w') as f:
        f.write(_strip_comments(source))


def parse_arguments():
    parser = argparse.ArgumentParser(description='Strip comments from C program.')

    def file_choices(choices, fname):
        ext = os.path.splitext(fname)[1][1:]
        if ext not in choices:
            parser.error(f"File doesn't end with one of {choices}")
        return fname

    parser.add_argument('filename',
                        type=lambda s: file_choices(("c", "h"), s),
                        help='The file to strip comments from')
    parser.add_argument('-o',
                        type=lambda s: file_choices(("c", "h"), s),
                        help='Output filename. If it is not specified, '
                        'the content of the original one will be replaced.')
    return parser.parse_args()


def main():
    args = parse_arguments()

    with open(args.filename, 'r') as f:
        source = f.read()

    filename = args.o if args.o else args.filename
    with open(filename, 'w') as f:
        f.write(_strip_comments(source))


if __name__ == '__main__':
    main()
