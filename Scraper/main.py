from GitRequests.ExtendedRepo import ExtendedRepo
from GitRequests.helper_func import get_trending_C_repos, get_size
import sys


def main(number_of_repos=10):
    repo_urls = get_trending_C_repos(number_of_repos)
    total_size = 0
    EXPECTED_SIZE = 1e10

    for repo_url in repo_urls:
        repo = ExtendedRepo(repo_url)
        repo.prepare_repo()
        total_size += get_size(repo.get_repo_dir())

        if total_size > EXPECTED_SIZE:
            return


if __name__ == '__main__':
    if len(sys.argv) > 1:
        main(int(sys.argv[1]))
    else:
        main()
