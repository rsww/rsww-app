import os
import requests
from math import ceil


def get_trending_C_repos(count):
    repos = dict()
    per_page = 100
    page_count = ceil(count / per_page)

    for page in range(1, page_count + 1):
        query_url = f"https://api.github.com/search/repositories?" \
                    f"q=language:C&sort=stars&order=desc&per_page={per_page}&page={page}"
        response = requests.get(query_url)
        items = [item['html_url'] for item in response.json().get('items', [])]
        repos.update(dict.fromkeys(items))

    return list(repos)[0:count]


def get_size(start_path='.'):
    total_size = 0
    for root, dirs, files in os.walk(start_path):
        for file in files:
            path = os.path.join(root, file)
            # skip if it is symbolic link
            if not os.path.islink(path):
                total_size += os.path.getsize(path)

    return total_size
