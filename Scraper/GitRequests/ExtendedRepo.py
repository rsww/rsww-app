import os
import shutil
import stat
from pathlib import Path
from git import Repo, RemoteProgress, GitCommandError
from Lexer.strip_comments import strip_comments


class ExtendedRepo(Repo):
    repos_base_path = None

    def __init__(self, url):
        self.url = url
        if not ExtendedRepo.repos_base_path:
            ExtendedRepo.repos_base_path = ExtendedRepo.get_repos_base_dir()
        self.repo_dir = self.__get_repo_dir()
        self.is_processed = self.__is_already_processed()

    def prepare_repo(self):
        if self.is_processed:
            print(f"{self.url} is already processed. Skipping...")
            return

        # Cloning Linux repo fails on Windows as it contains aux.c file
        if self.url == 'https://github.com/torvalds/linux':
            print("Cloning Linux repo fails on Windows as it contains aux.c file!")
            return

        self.clone_repo()
        self.filter_files(acceptable_extensions=('.c', '.h'))
        self.remove_comments()
        self.__rename_processed()

    def get_repo_dir(self):
        return self.repo_dir

    def __get_repo_dir(self):
        path = self.url.split('/')[-1]

        if path.endswith('.git'):
            path = path[0:-4]

        return os.path.join(ExtendedRepo.repos_base_path, path)

    def __is_already_processed(self):
        path = f"{self.repo_dir}_done"
        if not os.path.exists(path):
            return False

        self.repo_dir = path
        return True

    def __rename_processed(self):
        path = f"{self.repo_dir}_done"
        try:
            os.rename(self.repo_dir, path)
        except FileNotFoundError:
            # Mostly a case for Linux repo
            pass

    def clone_repo(self):
        if os.path.exists(self.repo_dir):
            print(f"Directory {self.repo_dir} already exists! Skipping cloning...")
            return

        try:
            print(f"Cloning {self.url} into {self.repo_dir} ...")
            Repo.clone_from(self.url, self.repo_dir, progress=self.CloneProgress())
        except GitCommandError:
            print("Script failed to clone that repository!")
            self.remove_entire_dir()

    def remove_entire_dir(self):
        def remove_readonly(func, path, _):
            try:
                os.chmod(path, stat.S_IWRITE)
                func(path)
            except FileNotFoundError:
                pass

        shutil.rmtree(self.repo_dir, onerror=remove_readonly)

    def filter_files(self, acceptable_extensions):
        print(f"Removing unnecessary files from {self.repo_dir} ...")
        self.__remove_files(acceptable_extensions)
        self.__remove_empty_dirs()

    def remove_comments(self):
        print(f"Removing comments from {self.repo_dir} ...")
        for root, dirs, files in os.walk(self.repo_dir):
            for file in files:
                path = os.path.join(root, file)
                strip_comments(path)

    def __remove_files(self, acceptable_extensions):
        for root, dirs, files in os.walk(self.repo_dir):
            for file in files:
                if not file.lower().endswith(acceptable_extensions):
                    path = os.path.join(root, file)
                    if os.path.islink(path):
                        os.unlink(path)
                    else:
                        os.chmod(path, stat.S_IWRITE)
                        os.remove(path)

    def __remove_empty_dirs(self):
        for root, dirs, files in os.walk(self.repo_dir):
            for directory in dirs:
                path = os.path.join(root, directory)
                try:
                    os.removedirs(path)
                except OSError:
                    # Non empty directory
                    pass

    @staticmethod
    def get_repos_base_dir():
        project_base_dir = Path(__file__).resolve().parents[1]
        return os.path.join(project_base_dir, "Repos")

    class CloneProgress(RemoteProgress):
        def update(self, op_code, cur_count, max_count=None, message=''):
            object_received_ratio = f"{cur_count:.0f}/{max_count:.0f}"
            percent_complete = f"{cur_count / (max_count or 100.0):.2%}"
            message = message if message else ""

            print(f"{object_received_ratio} {percent_complete} {message}")
