#!/usr/bin/env python

from pyspark.sql.types import *
import os
import pyspark
import zipfile


def convert_directory_to_sequence_file(directory, sc):
    repository_content = []
    
    for root, dirs, files in os.walk(directory):
        for file in files:
            f = open(root+'/'+file, encoding="ISO-8859-1")
            repository_content.append( (root+"/"+file, f.read()) )
            f.close()
            
    rdd = sc.parallelize(repository_content)
    rdd.map(lambda x: x).saveAsSequenceFile(directory+'_seq')

def main():
    sc = pyspark.SparkContext('local[*]')
    repo_dirs = [x for x in os.listdir() if os.path.isdir(x) and not x.startswith('.')]
    for repo_dir in repo_dirs:
        convert_directory_to_sequence_file(repo_dir, sc)
        compress_directory(repo_dir+'_seq')

def compress_directory(dir_name):
    zip_file_name = dir_name + ".zip"
    filePaths = retrieve_file_paths(dir_name)   
    zip_file = zipfile.ZipFile(zip_file_name, 'w')
    with zip_file:
        for file in filePaths:
            zip_file.write(file)


def retrieve_file_paths(dir_name):
    file_paths = []
    for root, directories, files in os.walk(dir_name):
        for filename in files:
            file_path = os.path.join(root, filename)
            file_paths.append(file_path)
    return file_paths
if __name__ == "__main__":
    main()
