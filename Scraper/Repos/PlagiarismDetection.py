#!/usr/bin/env python
from pyspark.sql import SparkSession
from pyspark.sql.functions import udf
from pyspark import SparkContext
from pyspark.sql.types import *
import os
import pyspark
import zipfile
import difflib

def main():
    sc = pyspark.SparkContext('local[*]')
    spark = SparkSession(sc)
    repo_dirs = [x for x in os.listdir() if os.path.isdir(x) and not x.startswith('.')]
    dataframes = []
    for repo_dir in repo_dirs:
        if "_done_seq" in repo_dir:
            dataframes.append(read_sequencefile_into_dataframe(sc, repo_dir))
    print(dataframes)

    quick_analysis_udf = udf(quick_analysis)
    analysis_udf = udf(analysis)

    for df in dataframes:
        print("QUICK:")
        df = df.withColumn("diff_quick", quick_analysis_udf(df["file_content"], df["file_content"]))
        df = df.filter(df["diff_quick"] > 0.5)
        df.show(10)

        print("NORMAL:")
        df = df.withColumn("diff_normal", analysis_udf(df["file_content"], df["file_content"]))
        df = df.filter(df["diff_normal"] > 0.5)
        df.show(10)

def read_sequencefile_into_dataframe(sc, sequencefile):
    sf = sc.sequenceFile(sequencefile)
    df = sf.toDF(["file_path", "file_content"])
    return df


def analysis(source, file):
    return difflib.SequenceMatcher(None, source, file).ratio()

def quick_analysis(source, file):
    return difflib.SequenceMatcher(None, source, file).quick_ratio()

if __name__ == "__main__":
    main()
