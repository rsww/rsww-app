#!/bin/bash
dotnet dev-certs https aspnetapp.pfx -p password
dotnet user-secrets -p ApiGateway/ApiGateway.csproj set "Kestrel:Certificates:Development:Password" "password"
dotnet user-secrets -p Microservices/AuthService/AuthService.csproj set "Kestrel:Certificates:Development:Password" "password"
