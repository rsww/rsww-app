﻿using AutoMapper;
using MediatR;
using ReportService.Common.Models;
using ReportService.Data;
using ReportService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ReportService.Queries.GetReports
{
    public class GetReportsQuery : IRequest<ResponseModel>
    {
        public Guid accountRef { get; private set; }

        public GetReportsQuery(Guid accountRef)
        {
            this.accountRef = accountRef;
        }
    }

    public class GetReportsQueryHandler : IRequestHandler<GetReportsQuery, ResponseModel>
    {
        private readonly DataContext dataContext;
        private readonly RepositoryDataContext repositoryDataContext;
        private readonly IMapper mapper;

        public GetReportsQueryHandler(DataContext dataContext, RepositoryDataContext repositoryDataContext, IMapper mapper)
        {
            this.dataContext = dataContext;
            this.repositoryDataContext = repositoryDataContext;
            this.mapper = mapper;
        }

        public async Task<ResponseModel> Handle(GetReportsQuery request, CancellationToken cancellationToken)
        {
            var reports = dataContext.Reports.Where(r => r.AccountRef == request.accountRef).ToList();
            var mappedReports = mapper.Map<List<Report>, List<GetReportsModel>>(reports);
            mappedReports.ForEach(r => r.RepositoryName = repositoryDataContext.Repositories.Find(r.RepositoryRef).Name);

            return new GetReportResponse(HttpStatusCode.OK, mappedReports);
        }
    }
}
