﻿using ReportService.Common.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ReportService.Queries.GetReports
{
    public class GetReportResponse : ResponseModel
    {
        public GetReportResponse(HttpStatusCode status, List<GetReportsModel> reports) : base(status)
        {
            Data = new GetReportsResponseData(reports);
        }

    }

    public class GetReportsResponseData
    {
        public List<GetReportsModel> Reports { get; private set; }

        public GetReportsResponseData(List<GetReportsModel> reports)
        {
            Reports = reports;
        }

    }
}