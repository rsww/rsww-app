﻿using AutoMapper;
using MediatR;
using ReportService.Common.Exceptions;
using ReportService.Common.Models;
using ReportService.Data;
using ReportService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ReportService.Queries.GetReport
{
    public class GetReportQuery : IRequest<ResponseModel>
    {
        public Guid AccountRef { get; private set; }
        public Guid ReportId { get; private set; }

        public GetReportQuery(Guid accountRef, Guid reportId)
        {
            AccountRef = accountRef;
            ReportId = reportId;
        }

    }

    public class GetReportQueryHandler : IRequestHandler<GetReportQuery, ResponseModel>
    {
        private readonly DataContext dataContext;

        public GetReportQueryHandler(DataContext dataContext, IMapper mapper)
        {
            this.dataContext = dataContext;
        }

        public async Task<ResponseModel> Handle(GetReportQuery request, CancellationToken cancellationToken)
        {
            var report = dataContext.Reports.Find(request.ReportId);
            if (report == null)
                throw new ReportNotFoundException();
            if (report.AccountRef != request.AccountRef)
                throw new NoReportAccessException();

            return new GetReportResponse(HttpStatusCode.OK, report);
        }
    }
}
