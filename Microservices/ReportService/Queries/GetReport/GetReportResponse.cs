﻿using ReportService.Common.Models;
using ReportService.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ReportService.Queries.GetReport
{
    public class GetReportResponse : ResponseModel
    {
        public GetReportResponse(HttpStatusCode status, Report report) : base(status)
        {
            Data = new GetReportResponseData(report);
        }

    }

    public class GetReportResponseData
    {
        public Report Report { get; private set; }

        public GetReportResponseData(Report report)
        {
            Report = report;
        }

    }
}