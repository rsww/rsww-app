﻿using AutoMapper;
using ReportService.Common.Models;
using ReportService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Profiles
{
    public class ReportProfile : Profile
    {
        public ReportProfile()
        {
            CreateMap<Report, GetReportsModel>();
            CreateMap<Repository, GetReportsModel>()
                .ForMember(dest => dest.RepositoryName, opt => opt.MapFrom(src => src.Name));
        }
    }
}
