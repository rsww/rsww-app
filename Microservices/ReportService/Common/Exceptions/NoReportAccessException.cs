﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Exceptions
{
    public class NoReportAccessException : Exception
    {
        private static readonly string MESSAGE = "User has no permission to access this report";

        public NoReportAccessException() : base(MESSAGE) { }
    }
}
