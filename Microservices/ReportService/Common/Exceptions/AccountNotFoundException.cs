﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Exceptions
{
    public class AccountNotFoundException : Exception
    {
        private static readonly string MESSAGE = "Account with given id doesn't exist";

        public AccountNotFoundException() : base(MESSAGE) { }

    }
}
