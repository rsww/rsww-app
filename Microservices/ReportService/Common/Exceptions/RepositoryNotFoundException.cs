﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Exceptions
{
    public class RepositoryNotFoundException : Exception
    {
        private static readonly string MESSAGE = "Repository with given id doesnt exist";

        public RepositoryNotFoundException() : base(MESSAGE) { }
    }
}
