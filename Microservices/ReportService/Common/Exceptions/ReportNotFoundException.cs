﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Exceptions
{
    public class ReportNotFoundException : Exception
    {
        private static readonly string MESSAGE = "Report with given id doesnt exist";

        public ReportNotFoundException() : base(MESSAGE) { }
    }
}
