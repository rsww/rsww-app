﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Models
{
    public class GetReportsModel
    {
        public Guid Id { get; set; }
        public DateTime AnalysisDate { get; set; }
        public Guid RepositoryRef { get; set; }
        public String RepositoryName { get; set; }
        public String Status { get; set; }

    }
}
