﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Models
{
    public class ReceivedMakeReportModel
    {
        public Guid AccountRef { get; set; }
        public List<Guid> RepositoryRefs { get; set; }
    }
}
