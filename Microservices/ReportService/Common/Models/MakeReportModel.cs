﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Models
{
    public class MakeReportModel
    {
        [FromBody]
        public List<Guid> RepositoryRefs { get; set; }
    }
}
