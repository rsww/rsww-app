﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Common.Services
{
    public interface IEmailService
    {
        Task Send(string to, string subject, string html);
    }

    public class EmailService : IEmailService
    {
        private readonly IConfiguration _config;
        private readonly string SMTPUser;
        private readonly string SMTPPassword;
        private readonly string SMTPHost;
        private readonly int SMTPPort;


        public EmailService(IConfiguration config)
        {
            _config = config;
            SMTPUser = _config.GetValue<string>("MailServiceConfig:Address");
            SMTPPassword = _config.GetValue<string>("MailServiceConfig:Password");
            SMTPHost = _config.GetValue<string>("MailServiceConfig:Host");
            SMTPPort = _config.GetValue<int>("MailServiceConfig:Port");
        }

        public async Task Send(string to, string subject, string content)
        {
            var email = new MimeMessage();
            email.From.Add(MailboxAddress.Parse(SMTPUser));
            email.To.Add(MailboxAddress.Parse(to));
            email.Subject = subject;
            email.Body = new TextPart(TextFormat.Text) { Text = content };

           // using var smtp = new SmtpClient();
           // smtp.Connect(SMTPHost, SMTPPort, SecureSocketOptions.StartTls);
           // smtp.Authenticate(SMTPUser, SMTPPassword);
           // smtp.Send(email);
           // smtp.Disconnect(true);
        }
    }
}
