﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReportService.Commands.MakeReport;
using ReportService.Common.Exceptions;
using ReportService.Common.Models;
using ReportService.Queries.GetReport;
using ReportService.Queries.GetReports;
using Sentry;

namespace ReportService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReportsController : Controller
    {
        private IMediator _mediator;

        public ReportsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("")]
        public async Task<IActionResult> MakeReport([FromHeader] Guid accountId, [FromBody] MakeReportModel model)
        {
            try
            {
                var response = _mediator.Send(new MakeReportCommand(accountId, model));
                return Ok(response.Result);
            }
            catch (AccountNotFoundException e)
            {
                SentrySdk.CaptureException(e);
                return NotFound(e.Message);
            }
            catch (RepositoryNotFoundException e)
            {
                SentrySdk.CaptureException(e);
                return NotFound(e.Message);
            }
        }

        [HttpGet("")]
        public IActionResult GetAllReportsForUser([FromHeader] Guid accountId)
        {
            var response = _mediator.Send(new GetReportsQuery(accountId));
            return Ok(response.Result);
        }

        [HttpGet("{reportId}")]
        public IActionResult GetReportById([FromHeader] Guid accountId, Guid reportId)
        {
            try
            {
                var response = _mediator.Send(new GetReportQuery(accountId, reportId));
                return Ok(response.Result);
            }
            catch (ReportNotFoundException e)
            {
                SentrySdk.CaptureException(e);
                return NotFound(e.Message);
            }
            catch (NoReportAccessException e)
            {
                SentrySdk.CaptureException(e);
                return Unauthorized(e.Message);
            }
        }

        [HttpGet]
        [Route("error")]
        public IActionResult ThrowError()
        {
            try
            {
                throw new NotImplementedException();
            }
            catch (NotImplementedException e)
            {
                SentrySdk.CaptureException(e);
                return Ok("Exception was artificially generated");
            }
        }
    }
}
