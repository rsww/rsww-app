﻿using ReportService.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Messaging.Sender
{
    public interface IMakeReportModelSender
    {
        public void SendMakeReportModel(ReceivedMakeReportModel receivedMakeReportModel);
    }
}
