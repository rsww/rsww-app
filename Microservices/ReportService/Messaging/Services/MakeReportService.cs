﻿using MediatR;
using ReportService.Commands.ReceivedMakeReport;
using ReportService.Common.Exceptions;
using ReportService.Common.Models;
using ReportService.Common.Services;
using ReportService.Data;
using ReportService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Messaging.Services
{
    public class MakeReportService : IMakeReportService
    {
        private readonly IMediator _mediator;

        public MakeReportService(IMediator mediator)
        {
            _mediator = mediator;
        }

        public async void MakeReport(ReceivedMakeReportModel receivedMakeReportModel)
        {
            var response = await _mediator.Send(new ReceivedMakeReportCommand(receivedMakeReportModel));
        }

    }
}
