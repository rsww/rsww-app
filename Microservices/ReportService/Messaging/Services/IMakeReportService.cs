﻿using ReportService.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Messaging.Services
{
    public interface IMakeReportService
    {
        void MakeReport(ReceivedMakeReportModel receivedMakeReportModel);
    }
}
