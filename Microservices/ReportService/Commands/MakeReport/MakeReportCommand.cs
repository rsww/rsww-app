﻿using MailKit.Net.Smtp;
using MailKit.Security;
using MediatR;
using MimeKit;
using MimeKit.Text;
using ReportService.Common.Exceptions;
using ReportService.Common.Models;
using ReportService.Common.Services;
using ReportService.Data;
using ReportService.Data.Entities;
using ReportService.Messaging.Sender;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ReportService.Commands.MakeReport
{
    public class MakeReportCommand : IRequest<ResponseModel>
    {
        public Guid accountId { get; private set; }
        public MakeReportModel makeReportModel { get; private set; }

        public MakeReportCommand(Guid accountId, MakeReportModel makeReportModel)
        {
            this.accountId = accountId;
            this.makeReportModel = makeReportModel;
        }

    }

    public class MakeReportCommandHandler : IRequestHandler<MakeReportCommand, ResponseModel>
    {
        private readonly IMakeReportModelSender makeReportModelSender;
        private readonly RepositoryDataContext repositoryDataContext;

        public MakeReportCommandHandler(IMakeReportModelSender makeReportModelSender, RepositoryDataContext repositoryDataContext)
        {
            this.makeReportModelSender = makeReportModelSender;
            this.repositoryDataContext = repositoryDataContext;
        }

        public async Task<ResponseModel> Handle(MakeReportCommand request, CancellationToken cancellationToken)
        {
            var makeReportModel = request.makeReportModel;

            foreach (Guid repositoryRef in makeReportModel.RepositoryRefs)
            {
                var repository = repositoryDataContext.Repositories.Find(repositoryRef);
                if (repository == null)
                    throw new RepositoryNotFoundException();
            }

            makeReportModelSender.SendMakeReportModel(new ReceivedMakeReportModel()
            {
                AccountRef = request.accountId,
                RepositoryRefs = request.makeReportModel.RepositoryRefs
            });
            return new MakeReportResponse(HttpStatusCode.OK, null);
        }

    }
}
