﻿using ReportService.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ReportService.Commands.MakeReport
{
    public class MakeReportResponse : ResponseModel
    {
        public MakeReportResponse(HttpStatusCode status, List<Guid> reportsIds) : base(status)
        {
            Data = new MakeReportResponseData(reportsIds);
        }

    }

    public class MakeReportResponseData
    {
        public List<Guid> ReportsIds { get; private set; }

        public MakeReportResponseData(List<Guid> reportsIds)
        {
            ReportsIds = reportsIds;
        }

    }
}