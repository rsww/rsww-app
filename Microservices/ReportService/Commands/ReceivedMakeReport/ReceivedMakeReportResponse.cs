﻿using ReportService.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ReportService.Commands.ReceivedMakeReport
{
    public class ReceivedMakeReportResponse : ResponseModel
    {
        public ReceivedMakeReportResponse(HttpStatusCode status, List<Guid> reportsIds) : base(status)
        {
            Data = new ReceivedMakeReportResponseData(reportsIds);
        }

    }

    public class ReceivedMakeReportResponseData
    {
        public List<Guid> ReportsIds { get; private set; }

        public ReceivedMakeReportResponseData(List<Guid> reportsIds)
        {
            ReportsIds = reportsIds;
        }

    }
}
