﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using ReportService.Common.Exceptions;
using ReportService.Common.Models;
using ReportService.Common.Services;
using ReportService.Data;
using ReportService.Data.Entities;
using Sentry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace ReportService.Commands.ReceivedMakeReport
{
    public class ReceivedMakeReportCommand : IRequest
    {
        public ReceivedMakeReportModel receivedMakeReportModel { get; set; }

        public ReceivedMakeReportCommand(ReceivedMakeReportModel receivedMakeReportModel)
        {
            this.receivedMakeReportModel = receivedMakeReportModel;
        }

    }

    public class ReceivedMakeReportCommandHandler : IRequestHandler<ReceivedMakeReportCommand>
    {
        private IServiceScopeFactory serviceScopeFactory;
        private IHub sentryHub;

        public ReceivedMakeReportCommandHandler(IServiceScopeFactory serviceScopeFactory, IHub sentryHub)
        {
            this.serviceScopeFactory = serviceScopeFactory;
            this.sentryHub = sentryHub;
        }

        public async Task<Unit> Handle(ReceivedMakeReportCommand request, CancellationToken cancellationToken)
        {
            var transaction = SentrySdk.StartTransaction(
              "ReceivedMakeReportCommand-transaction",
              "Handle-operation"
            );

            using (var scope = serviceScopeFactory.CreateScope())
            {
                var scopedServices = scope.ServiceProvider;

                var emailService = scopedServices.GetRequiredService<EmailService>();
                var dataContext = scopedServices.GetRequiredService<DataContext>();
                var authDataContext = scopedServices.GetRequiredService<AuthDataContext>();
                var repositoryDataContext = scopedServices.GetRequiredService<RepositoryDataContext>();

                 var receivedMakeReportModel = request.receivedMakeReportModel;

                var account = authDataContext.Accounts.Find(receivedMakeReportModel.AccountRef.ToString());
                if (account == null)
                    throw new AccountNotFoundException();

                var newReportsIds = new List<Guid>();
                foreach (Guid repositoryRef in receivedMakeReportModel.RepositoryRefs)
                {
                    var repository = repositoryDataContext.Repositories.Find(repositoryRef);
                    if (repository == null)
                        throw new RepositoryNotFoundException();

                    Report newReport = new Report()
                    {
                        Id = Guid.NewGuid(),
                        AnalysisDate = DateTime.Now,
                        RepositoryRef = repositoryRef,
                        AccountRef = receivedMakeReportModel.AccountRef,
                        Status = "In Progress",
                        Content = ""
                    };

                    dataContext.Reports.Add(newReport);
                    dataContext.SaveChanges();

                    var span = transaction.StartChild("Handle-analysis-operation");
                    /* 
                     * ORDER ANALYSIS HERE
                     * The status and content will need to be changed after analysis
                     */
                    span.Finish(); // Mark the span as finished

                    await emailService.Send(account.Email, "Lorem Ipsum", "Lorem Ipsum");
                    newReportsIds.Add(newReport.Id);
                }

                transaction.Finish();

                return Unit.Value;
            }
        }

    }
}
