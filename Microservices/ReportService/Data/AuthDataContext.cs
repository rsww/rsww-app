﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Data
{
    public class AuthDataContext : IdentityDbContext<Entities.Account>
    {
        public DbSet<Entities.Account> Accounts { get; set; }

        public AuthDataContext(DbContextOptions<AuthDataContext> options) : base(options) { }
    }
}
