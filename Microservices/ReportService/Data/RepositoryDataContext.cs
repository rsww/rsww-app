﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Data
{
    public class RepositoryDataContext : DbContext
    {
        public DbSet<Entities.Repository> Repositories { get; set; }

        public RepositoryDataContext(DbContextOptions<RepositoryDataContext> options) : base(options) { }

    }
}
