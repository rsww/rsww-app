﻿using Microsoft.EntityFrameworkCore;

namespace ReportService.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Entities.Report> Reports { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

    }
}
