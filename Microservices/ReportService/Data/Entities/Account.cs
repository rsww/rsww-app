﻿using System;
using Microsoft.AspNetCore.Identity;

namespace ReportService.Data.Entities
{
    public class Account : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
