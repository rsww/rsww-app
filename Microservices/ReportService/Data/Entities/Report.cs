﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReportService.Data.Entities
{
    public class Report
    {
        public Guid Id { get; set; }
        public DateTime AnalysisDate { get; set; }
        public Guid RepositoryRef { get; set; }
        public Guid AccountRef { get; set; }

        public String Status { get; set; }
        public String Content { get; set; }
    }
}
