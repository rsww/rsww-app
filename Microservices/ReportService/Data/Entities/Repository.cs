﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace ReportService.Data.Entities
{
    public class Repository
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public Boolean IsBase { get; set; }
        public DateTime LastCommitDate { get; set; }
        public Guid AccountRef { get; set; }
    }
}
