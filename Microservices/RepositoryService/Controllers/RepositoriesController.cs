﻿using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Minio;
using Minio.DataModel;
using RepositoryService.Commands.AddRepository;
using RepositoryService.Common.Exceptions;
using RepositoryService.Common.Models;
using RepositoryService.GerRepositories.Queries;
using RepositoryService.GerRepository.Queries;
using Sentry;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RepositoriesController : Controller
    {
        private IMediator _mediator;

        public RepositoriesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost("")]
        public async Task<IActionResult> AddRepository([FromHeader] Guid accountId, [FromForm] AddRepositoryModel model)
        {
            var response = _mediator.Send(new AddRepositoryCommand(accountId, model));
            return Ok(response.Result);
        }

        [HttpPut("{repositoryId}")]
        public async Task<IActionResult> ChangeRepositoryBaseStatus([FromHeader] Guid accountId, Guid repositoryId, Boolean isBase)
        {
            try
            {
                var response = await _mediator.Send(new ChangeRepositoryBaseStatusCommand(accountId, repositoryId, isBase));
                return Ok(response);
            }
            catch (RepositoryNotFoundException e)
            {
                SentrySdk.CaptureException(e);
                return NotFound(e.Message);
            }
            catch (NoRepositoryModifyPermissionException e)
            {
                SentrySdk.CaptureException(e);
                return Unauthorized(e.Message);
            }
        }

        [HttpGet("")]
        public IActionResult GetAllRepositoriesForUser()
        {
            var response = _mediator.Send(new GetRepositoriesQuery());    
            return Ok(response.Result);
        }

        [HttpGet("{repositoryId}")]
        public IActionResult GetRepositoryById(Guid repositoryId)
        {
            try
            {
                var response = _mediator.Send(new GetRepositoryQuery(repositoryId));
                return Ok(response.Result);
            }
            catch (RepositoryNotFoundException e)
            {
                SentrySdk.CaptureException(e);
                return NotFound(e.Message);
            }
        }

    }
}
