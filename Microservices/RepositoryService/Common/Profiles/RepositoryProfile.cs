﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using RepositoryService.Common.Models;
using RepositoryService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Common.Profiles
{
    public class RepositoryProfile : Profile
    {
        public RepositoryProfile()
        {
            CreateMap<Repository, GetRepositoriesModel>();
            CreateMap<Repository, GetRepositoryModel>();
            CreateMap<FileModel, GetFileModel>();
        }
    }
}
