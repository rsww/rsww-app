﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Common.Models
{
    public class FileModel
    {
        public Guid Id { get; set; }
        public String FileName { set; get; }
        public byte[] Content { set; get; }
        public Guid RepositoryRef { set; get; }
    }
}
