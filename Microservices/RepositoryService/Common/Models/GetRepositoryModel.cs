﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Common.Models
{
    public class GetRepositoryModel
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public Boolean IsBase { get; set; }
        public DateTime LastCommitDate { get; set; }
        public Guid AccountRef { get; set; }
        public List<GetFileModel> Files { get; set; }
    }
}
