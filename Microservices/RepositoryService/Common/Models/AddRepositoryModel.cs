﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Common.Models
{
    public class AddRepositoryModel
    {
        [Required]
        public String Name { get; set; }
        [Required]
        public Boolean IsBase { get; set; }
        [Required]
        public DateTime LastCommitDate { get; set; }
        [Required]
        public List<IFormFile> Files { set; get; }
    }
}
