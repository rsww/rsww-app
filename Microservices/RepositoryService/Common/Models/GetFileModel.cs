﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Common.Models
{
    public class GetFileModel
    {
        public Guid Id { get; set; }
        public String FileName { set; get; }
    }
}
