﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RepositoryService.Common.Exceptions
{
    public class NoRepositoryModifyPermissionException : Exception
    {
        private static readonly string MESSAGE = "User has no permission to modify this repository";

        public NoRepositoryModifyPermissionException() : base(MESSAGE) { }
    }
}
