﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using RepositoryService.Common.Models;

namespace RepositoryService.Data.Entities
{
    public class Repository
    {
        public Guid Id { get; set; }
        public String Name { get; set; }
        public Boolean IsBase { get; set; }
        public DateTime LastCommitDate { get; set; }
        public Guid AccountRef { get; set; }
    }
}
