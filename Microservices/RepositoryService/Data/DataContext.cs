﻿using Microsoft.EntityFrameworkCore;

namespace RepositoryService.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Entities.Repository> Repositories { get; set; }
        public DbSet<Common.Models.FileModel> Files { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

    }
}
