﻿using RepositoryService.Common.Models;
using System;
using System.Net;

namespace RepositoryService.Commands.AddRepository
{
    public class AddRepositoryResponse : ResponseModel
    {
        public AddRepositoryResponse(HttpStatusCode status, Guid repositoryId) : base(status)
        {
            Data = new AddRepositoryResponseData(repositoryId);
        }

    }

    public class AddRepositoryResponseData
    {
        public Guid RepositoryId { get; private set; }

        public AddRepositoryResponseData(Guid repositoryId)
        {
            RepositoryId = repositoryId;
        }

    }
}
