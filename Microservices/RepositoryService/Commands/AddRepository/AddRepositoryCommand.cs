﻿using RepositoryService.Common.Models;
using MediatR;
using RepositoryService.Data;
using RepositoryService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using System.IO;

namespace RepositoryService.Commands.AddRepository
{
    public class AddRepositoryCommand : IRequest<ResponseModel>
    {
        public String Name { get; private set; }
        public Boolean IsBase { get; private set; }
        public DateTime LastCommitDate { get; private set; }
        public Guid AccountRef { get; private set; }
        public List<IFormFile> Files { get; private set; }

        public AddRepositoryCommand(Guid accountRef, AddRepositoryModel model)
        {
            Name = model.Name;
            IsBase = model.IsBase;
            LastCommitDate = model.LastCommitDate;
            AccountRef = accountRef;
            Files = model.Files;
        }

    }

    public class AddRepositoryCommandHandler : IRequestHandler<AddRepositoryCommand, ResponseModel>
    {
        private readonly IMapper mapper;
        private readonly DataContext dataContext;

        public AddRepositoryCommandHandler(IMapper mapper, DataContext dataContext)
        {
            this.mapper = mapper;
            this.dataContext = dataContext;
        }

        public async Task<ResponseModel> Handle(AddRepositoryCommand request, CancellationToken cancellationToken)
        {
            Repository newRepository = new Repository()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                IsBase = request.IsBase,
                LastCommitDate = request.LastCommitDate,
                AccountRef = request.AccountRef,
            };

            var fileModels = new List<FileModel>();

            foreach (var file in request.Files)
            {
                if (file.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        file.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        fileModels.Add(new FileModel()
                        {
                            Id = Guid.NewGuid(),
                            FileName = file.FileName,
                            Content = fileBytes,
                            RepositoryRef = newRepository.Id
                        });
                    }
                }
            }

            dataContext.Repositories.Add(newRepository);
            dataContext.Files.AddRange(fileModels);
            dataContext.SaveChanges();

            return new AddRepositoryResponse(HttpStatusCode.OK, newRepository.Id);
        }

    }
}
