﻿using MediatR;
using RepositoryService.Commands.ChangeRepositoryBaseStatus;
using RepositoryService.Common.Exceptions;
using RepositoryService.Common.Models;
using RepositoryService.Data;
using System;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace RepositoryService.Controllers
{
    public class ChangeRepositoryBaseStatusCommand : IRequest<ResponseModel>
    {
        public Guid AccountId { get; private set; }
        public Guid RepositoryId { get; private set; }
        public bool IsBase { get; private set; }

        public ChangeRepositoryBaseStatusCommand(Guid accountId, Guid repositoryId, bool isBase)
        {
            AccountId = accountId;
            RepositoryId = repositoryId;
            IsBase = isBase;
        }

    }

    public class ChangeRepositoryBaseStatusCommandHandler : IRequestHandler<ChangeRepositoryBaseStatusCommand, ResponseModel>
    {
        private readonly DataContext _dataContext;

        public ChangeRepositoryBaseStatusCommandHandler(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task<ResponseModel> Handle(ChangeRepositoryBaseStatusCommand request, CancellationToken cancellationToken)
        {
            var repository = _dataContext.Repositories.Where(r => r.Id == request.RepositoryId).FirstOrDefault();
            if (repository == null)
                throw new RepositoryNotFoundException();

            if (repository.AccountRef != request.AccountId)
                throw new NoRepositoryModifyPermissionException();

            
            _dataContext.SaveChanges();

            return new ChangeRepositoryBaseStatusResponse(HttpStatusCode.OK, repository);
        }

    }
}