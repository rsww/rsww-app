﻿using RepositoryService.Common.Models;
using RepositoryService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RepositoryService.Commands.ChangeRepositoryBaseStatus
{
    public class ChangeRepositoryBaseStatusResponse : ResponseModel
    {
        public ChangeRepositoryBaseStatusResponse(HttpStatusCode status, Repository repository) : base(status)
        {
            Data = new ChangeRepositoryBaseStatusResponseData(repository);
        }

    }

    public class ChangeRepositoryBaseStatusResponseData
    {
        public Repository Repository { get; private set; }

        public ChangeRepositoryBaseStatusResponseData(Repository repository)
        {
            Repository = repository;
        }

    }
}
