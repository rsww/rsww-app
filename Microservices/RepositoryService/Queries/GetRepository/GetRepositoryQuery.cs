﻿using AutoMapper;
using MediatR;
using RepositoryService.Common.Exceptions;
using RepositoryService.Common.Models;
using RepositoryService.Data;
using RepositoryService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace RepositoryService.GerRepository.Queries
{
    public class GetRepositoryQuery : IRequest<ResponseModel>
    {
        public Guid RepositoryId { get; private set; }

        public GetRepositoryQuery(Guid repositoryId)
        {
            RepositoryId = repositoryId;
        }

    }

    public class GetRepositoryQueryHandler : IRequestHandler<GetRepositoryQuery, ResponseModel>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;
        public GetRepositoryQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<ResponseModel> Handle(GetRepositoryQuery request, CancellationToken cancellationToken)
        {
            var repository = _dataContext.Repositories.Find(request.RepositoryId);
            if (repository == null)
                throw new RepositoryNotFoundException();

            var files = _dataContext.Files.Where(f => f.RepositoryRef == repository.Id).ToList();

            var mappedRepository = _mapper.Map<Repository, GetRepositoryModel>(repository);
            var mappedFiles = _mapper.Map<List<FileModel>, List<GetFileModel>>(files);

            mappedRepository.Files = mappedFiles;

            return new GetRepositoryResponse(HttpStatusCode.OK, mappedRepository);
        }
    }
}
