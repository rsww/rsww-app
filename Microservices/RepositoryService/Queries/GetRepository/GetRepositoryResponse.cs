﻿using RepositoryService.Common.Models;
using RepositoryService.Data.Entities;
using System.Collections.Generic;
using System.Net;

namespace RepositoryService.GerRepository.Queries
{
    public class GetRepositoryResponse : ResponseModel
    {
        public GetRepositoryResponse(HttpStatusCode status, GetRepositoryModel repository) : base(status)
        {
            Data = new GetRepositoryResponseData(repository);
        }

    }

    public class GetRepositoryResponseData
    {
        public GetRepositoryModel Repository { get; private set; }

        public GetRepositoryResponseData(GetRepositoryModel repository)
        {
            Repository = repository;
        }

    }
}