﻿using AutoMapper;
using MediatR;
using RepositoryService.Common.Models;
using RepositoryService.Data;
using RepositoryService.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace RepositoryService.GerRepositories.Queries
{
    public class GetRepositoriesQuery : IRequest<ResponseModel>
    {
    }

    public class GetRepositoriesQueryHandler : IRequestHandler<GetRepositoriesQuery, ResponseModel>
    {
        private readonly DataContext _dataContext;
        private readonly IMapper _mapper;

        public GetRepositoriesQueryHandler(DataContext dataContext, IMapper mapper)
        {
            _dataContext = dataContext;
            _mapper = mapper;
        }

        public async Task<ResponseModel> Handle(GetRepositoriesQuery request, CancellationToken cancellationToken)
        {
            var repositories = _dataContext.Repositories.ToList();
            var mappedRepositories = _mapper.Map<List<Repository>, List<GetRepositoriesModel>>(repositories);
            return new GetRepositoriesResponse(HttpStatusCode.OK, mappedRepositories);
        }
    }
}
