﻿using RepositoryService.Common.Models;
using RepositoryService.Data.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace RepositoryService.GerRepositories.Queries
{
    public class GetRepositoriesResponse : ResponseModel
    {
        public GetRepositoriesResponse(HttpStatusCode status, List<GetRepositoriesModel> repositories) : base(status)
        {
            Data = new GetRepositoriesResponseData(repositories);
        }

    }

    public class GetRepositoriesResponseData
    {
        public List<GetRepositoriesModel> Repositories { get; private set; }

        public GetRepositoriesResponseData(List<GetRepositoriesModel> repositories)
        {
            Repositories = repositories;
        }

    }
}