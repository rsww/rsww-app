import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/Home.vue'
import Repositories from '@/components/Repositories.vue'
import Repository from '@/components/Repository.vue'
import Login from '@/components/Login.vue'
import Register from '@/components/Register.vue'
import Reports from "@/components/Reports";
import Report from "@/components/Report";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/repositories',
        name: 'Repositories',
        component: Repositories
    },
    {
        path: '/repository/:id',
        name: 'Repository',
        component: Repository
    },
    {
        path: '/reports',
        name: 'Reports',
        component: Reports
    },
    {
        path: '/report/:id',
        name: 'Report',
        component: Report
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
    {
        path: '/register',
        name: 'Register',
        component: Register
    },
    {
        path: '*',
        redirect: '/'
    }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

export default router
