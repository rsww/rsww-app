#!/bin/bash
dotnet dev-certs https -ep ${HOME}/.aspnet/https/aspnetapp.pfx -p crypticpassword
dotnet user-secrets -p ApiGateway/ApiGateway.csproj set "Kestrel:Certificates:Development:Password" "password"
dotnet user-secrets -p Microservices/AuthService/AuthService.csproj set "Kestrel:Certificates:Development:Password" "password"
